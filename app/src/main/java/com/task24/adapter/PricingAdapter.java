package com.task24.adapter;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.task24.R;
import com.task24.util.DividerView;
import com.task24.util.textview.TextViewHelveticaLight;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PricingAdapter extends RecyclerView.Adapter<PricingAdapter.SimpleViewHolder> {

    private Context context;

    public PricingAdapter(Context context) {
        this.context = context;
    }

    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.price_list_item, parent, false);
        return new SimpleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final SimpleViewHolder holder, final int position) {
        if (position == 0) {
            holder.tvTitle.setText("VIP");
            holder.tvPrice.setText("$3.90");
            holder.tvDescription.setText("150 hours or more/Month");
        } else if (position == 1) {
            holder.tvTitle.setText("Plus");
            holder.tvPrice.setText("$4.90");
            holder.tvDescription.setText("90 hours or more/Month");
        } else if (position == 2) {
            holder.tvTitle.setText("By The Hour");
            holder.tvPrice.setText("$5.90");
            holder.tvDescription.setText("By The Hour");
        }
    }

    @Override
    public int getItemCount() {
        return 3;
    }

    class SimpleViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_title)
        TextViewHelveticaLight tvTitle;
        @BindView(R.id.tv_price)
        TextViewHelveticaLight tvPrice;
        @BindView(R.id.tv_description)
        TextViewHelveticaLight tvDescription;

        SimpleViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(context, getAdapterPosition() + "", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
}
