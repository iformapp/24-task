package com.task24;

import android.app.Application;
import android.content.Context;
import android.net.ConnectivityManager;
import android.widget.Toast;

import com.task24.api.ApiClient;
import com.task24.api.ApiInterface;
import com.task24.model.ServicesModel;
import com.task24.util.Constants;
import com.task24.util.Preferences;

import io.intercom.android.sdk.Intercom;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Task24Application extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        Intercom.initialize(this, "android_sdk-59492598d6d65b76b06a22396d76da69dc6d3431", "ck9u1r6v");
        Intercom.client().registerUnidentifiedUser();

        Preferences.writeString(this, Constants.SERVICE_NAME, "");
        getAllServices();
    }

    public void getAllServices() {
        if (!isNetworkConnected())
            return;

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<ServicesModel> call = apiInterface.getServices();
        call.enqueue(new Callback<ServicesModel>() {
            @Override
            public void onResponse(Call<ServicesModel> call, Response<ServicesModel> response) {
                ServicesModel servicesModel = response.body();
                if (servicesModel != null && servicesModel.flag == 1) {
                    Preferences.saveServices(getApplicationContext(), servicesModel.data.services);
                }
            }

            @Override
            public void onFailure(Call<ServicesModel> call, Throwable t) {
                //failureError("services load failed");
            }
        });
    }

    public boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm != null && cm.getActiveNetworkInfo() != null) {
            return true;
        }
        Toast.makeText(this, "connect to internet", Toast.LENGTH_SHORT).show();
        return false;
    }
}
