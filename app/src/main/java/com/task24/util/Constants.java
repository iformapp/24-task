package com.task24.util;

public class Constants {

    //API URL PARAMS
    public static final String BASE_URL = "http://www.24task.com/";
    public static final String URL = "api/";
    public static final String SERVICES = URL + "services.json";
    public static final String ADD_FREE_TRIAL = URL + "freetrials/add.json";
    public static final String PRICE_CALCULATION = URL + "services/getPriceCalculation.json";
    public static final String PRIVACY = BASE_URL + "#/privacy-policy";
    public static final String TERMS_USE = BASE_URL + "#/terms-of-use";
    public static final String FAQS = BASE_URL + "#/faqs";
    public static final String ABOUTUS = BASE_URL + "#/about-us";

    //Api parameter
    public static final String PAPER_TYPES = "paperTypes";
    public static final String ACADEMIC_TYPES = "getAcademicTypes";
    public static final String DISCIPLIN_TYPES = "getDisciplines";
    public static final String FORMATED_STYLE_TYPES = "getFormatedStyle";
    public static final String SUBJECTS_TYPES = "subjects";

    public static final int TAB_HOME = 0;
    public static final int TAB_CHAT = 1;
    public static final int TAB_FREE_TRIAL = 2;
    public static final int TAB_CALCULATE = 3;
    public static final int TAB_MORE = 4;

    public static final String SFTEXT_REGULAR = "font/SanFranciscoText-Regular.otf";
    public static final String SFTEXT_BOLD = "font/SanFranciscoText-Bold.otf";
    public static final String SFTEXT_MEDIUM = "font/SanFranciscoText-Medium.otf";
    public static final String SFTEXT_SEMIBOLD = "font/SanFranciscoText-Semibold.otf";
    public static final String SFDISPLAY_BOLD = "font/SF-Pro-Display-Bold.otf";
    public static final String SFDISPLAY_REGULAR = "font/SF-Pro-Display-Regular.otf";
    public static final String HELVETICA = "font/HelveticaNeue.ttf";
    public static final String HELVETICA_LIGHT = "font/HelveticaNeue Light.ttf";

    public static final String FCM_TOKEN = "fcm_token";
    public static final String POLICY_URL = "policy_url";
    public static final String SCREEN_NAME = "screen _name";

    public static final String PREF_SERVICES = "services";
    public static final String SERVICE_NAME = "service_name";
    public static final String SERVICE_TYPE = "service_type";
    public static final String HIRE = "hire";
    public static final String WHY_US = "whyus";
    public static final String HOW_IT_WORKS = "howitworks";

    public static final int NOTIFICATION_ID = 100;
    public static final int NOTIFICATION_ID_BIG_IMAGE = 101;
}
