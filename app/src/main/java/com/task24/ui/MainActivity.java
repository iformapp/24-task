package com.task24.ui;

import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.widget.TabHost;

import com.task24.R;
import com.task24.ui.calculate.CalculateActivity;
import com.task24.ui.chat.ChatActivity;
import com.task24.ui.freetrial.FreeTrialActivity;
import com.task24.ui.home.HomeActivity;
import com.task24.ui.more.MoreActivity;
import com.task24.util.Constants;
import com.task24.util.Preferences;

public class MainActivity extends TabActivity implements TabHost.OnTabChangeListener {

    private TabHost mTabHost;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mTabHost = findViewById(android.R.id.tabhost);

        setTab("home", R.drawable.tab_home, HomeActivity.class);
        setTab("chat", R.drawable.tab_chat, ChatActivity.class);
        setTab("plus", R.drawable.tab_plus, FreeTrialActivity.class);
        setTab("calculate", R.drawable.tab_calculate, CalculateActivity.class);
        setTab("more", R.drawable.tab_more, MoreActivity.class);

        mTabHost.setOnTabChangedListener(this);

        if (getIntent() != null) {
            int screen = getIntent().getIntExtra(Constants.SCREEN_NAME, 0);
            mTabHost.setCurrentTab(screen);
        }
    }

    public void setTab(String tag, int drawable, Class<?> activityClass) {
        TabHost.TabSpec tabSpec = mTabHost.newTabSpec(tag)
                .setIndicator("", ContextCompat.getDrawable(this, drawable))
                .setContent(new Intent(this, activityClass));
        mTabHost.addTab(tabSpec);
    }

    @Override
    public void onTabChanged(String tabId) {

    }

    public void gotoMainActivity(int screen) {
        Intent i = new Intent(this, MainActivity.class);
        i.putExtra(Constants.SCREEN_NAME, screen);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
        finish();
        overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out);
    }

    public void redirectActivity(Class<?> activityClass) {
        startActivity(new Intent(this, activityClass));
        overridePendingTransition(R.anim.trans_left_in, R.anim.trans_left_out);
    }
}
