package com.task24.ui.more;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.task24.R;
import com.task24.ui.BaseActivity;
import com.task24.ui.policy.PolicyActivity;
import com.task24.util.Constants;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class MoreActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_more);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.rl_whatwecan, R.id.rl_whyus, R.id.rl_faqs, R.id.rl_howitworks, R.id.rl_policy})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.rl_whatwecan:
                Intent i = new Intent(this, ServicesActivity.class);
                i.putExtra(Constants.SERVICE_TYPE, Constants.HIRE);
                startActivity(i);
                openToLeft();
                break;
            case R.id.rl_whyus:
                i = new Intent(this, ServicesActivity.class);
                i.putExtra(Constants.SERVICE_TYPE, Constants.WHY_US);
                startActivity(i);
                openToLeft();
                break;
            case R.id.rl_faqs:
                redirectUsingCustomTab(Constants.FAQS);
                break;
            case R.id.rl_howitworks:
                i = new Intent(this, ServicesActivity.class);
                i.putExtra(Constants.SERVICE_TYPE, Constants.HOW_IT_WORKS);
                startActivity(i);
                openToLeft();
                break;
            case R.id.rl_policy:
                redirectActivity(PolicyActivity.class);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (getParent() != null)
            redirectTab(Constants.TAB_HOME);
        else
            super.onBackPressed();
    }
}
