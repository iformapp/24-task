package com.task24.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ServicesModel extends GeneralModel {

    @Expose
    @SerializedName("data")
    public Data data;


    public static class Data {
        @Expose
        @SerializedName("services")
        public List<Services> services;
    }

    public static class Services {
        @Expose
        @SerializedName("timestamp")
        public String timestamp;
        @Expose
        @SerializedName("status")
        public boolean status;
        @Expose
        @SerializedName("order_by")
        public int orderBy;
        @Expose
        @SerializedName("on_home")
        public boolean onHome;
        @Expose
        @SerializedName("name")
        public String name;
        @Expose
        @SerializedName("id")
        public int id;

        public boolean isSelected;
    }
}
