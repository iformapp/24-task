package com.task24.api;

import com.task24.model.GeneralModel;
import com.task24.model.GetPrice;
import com.task24.model.ServicesModel;
import com.task24.util.Constants;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface ApiInterface {

    @GET(Constants.SERVICES)
    Call<ServicesModel> getServices();

    @FormUrlEncoded
    @POST(Constants.ADD_FREE_TRIAL)
    Call<GeneralModel> addFreeTrial(@Field("fullname") String fullname, @Field("phone") String phone,
                                    @Field("website") String website, @Field("description") String description,
                                    @Field("email") String email, @Field("subject") String subject);

    @FormUrlEncoded
    @POST(Constants.PRICE_CALCULATION)
    Call<GetPrice> getPriceCalculation(@Field("noOfHours") String noOfHours);
}
